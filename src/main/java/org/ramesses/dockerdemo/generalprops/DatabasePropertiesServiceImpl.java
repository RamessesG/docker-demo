package org.ramesses.dockerdemo.generalprops;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
class DatabasePropertiesServiceImpl implements DatabasePropertiesService {

  @Value("${spring.datasource.url:mock-endpoint}")
  private String endpoint;
  
  @Value("${spring.datasource.username:mock-username}")
  private String username;
  
  @Value("${spring.datasource.password:mock-userpassword}")
  private String password;

  @Override
  public String getEndpoint() {
    return endpoint;
  }

  @Override
  public String getUserName() {
    return username;
  }

  @Override
  public String getUserPassword() {
    return password;
  }
  
  
}
