package org.ramesses.dockerdemo.generalprops;

public interface DatabasePropertiesService {
  
  String getEndpoint();
  
  String getUserName();
  
  String getUserPassword();

}
