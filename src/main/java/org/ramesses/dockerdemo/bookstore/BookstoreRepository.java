package org.ramesses.dockerdemo.bookstore;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookstoreRepository extends JpaRepository<Book, Long> {
  
}
