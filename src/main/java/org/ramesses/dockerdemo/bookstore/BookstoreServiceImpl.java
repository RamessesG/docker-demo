package org.ramesses.dockerdemo.bookstore;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class BookstoreServiceImpl implements BookstoreService {

  private BookstoreRepository repository;

  @Autowired
  public BookstoreServiceImpl(BookstoreRepository repository) {
    this.repository = repository;
  }

  @Override
  public Optional<Book> getBookById(Long id) {
    return repository.findById(id);
  }

}
