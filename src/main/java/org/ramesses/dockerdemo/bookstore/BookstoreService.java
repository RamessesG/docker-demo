package org.ramesses.dockerdemo.bookstore;

import java.util.Optional;

public interface BookstoreService {
  
  Optional<Book> getBookById(Long id);

}
