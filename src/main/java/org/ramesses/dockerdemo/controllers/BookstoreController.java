package org.ramesses.dockerdemo.controllers;

import java.util.Optional;
import org.ramesses.dockerdemo.bookstore.Book;
import org.ramesses.dockerdemo.bookstore.BookstoreService;
import org.ramesses.dockerdemo.controllers.exceptions.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookstoreController {
  
  private BookstoreService service;
  
  public BookstoreController(BookstoreService service) {
    this.service = service;
  }
  
  @GetMapping("/books/{bookId}")
  public Book getBook(@PathVariable Long bookId) {
    Optional<Book> book = service.getBookById(bookId);
    if (!book.isPresent()) {
      throw new ResourceNotFoundException(bookId.toString());
    }
    return book.get();
  }

}
