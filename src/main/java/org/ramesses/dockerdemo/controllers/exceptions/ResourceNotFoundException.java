package org.ramesses.dockerdemo.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "The requested resource was not found")
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1277281573072463172L;
  
  public ResourceNotFoundException(String id) {
    super(String.format("Resource with ID %s not found", id));
  }

}
