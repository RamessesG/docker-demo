FROM openjdk:8-jre-alpine

ENV SPRING_DATASOURCE_URL="jdbc:mysql://192.168.0.12:9306/bookstore"
ENV SPRING_DATASOURCE_USERNAME="root"
ENV SPRING_DATASOURCE_PASSWORD="so-secret"

COPY ./target/docker-demo-0.0.1-SNAPSHOT.jar /usr/src/docker-demo/
WORKDIR /usr/src/docker-demo/
EXPOSE 8080
CMD ["java", "-jar", "docker-demo-0.0.1-SNAPSHOT.jar"]
